<?php get_header(); ?>
<div class="enigma_header_breadcrum_title">	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php _e('Nic nenalezeno','weblizar'); ?></h1>
				<ul class="breadcrumb">
					<li><a href="<?php echo home_url( '/' ); ?>"><?php _e('Home','weblizar'); ?></a></li>
					<li><?php _e('Nic nenalezeno','weblizar'); ?></li>
				
				</ul>
			</div>
		</div>
	</div>	
</div>
<div class="container">
	<div class="row enigma_blog_wrapper">
		<div class="col-md-12 hc_404_error_section">
			<div class="error_404">
				<h4><?php _e('Omlouváme se, ale nic relevantního nebylo nalezeno','weblizar'); ?></h4>
				<p><?php _e('Můžete využít menu nebo vyhledávání napravo, případně přejít na hlavní stránku.','weblizar'); ?></p>
				<p><a href="<?php echo home_url( '/' ); ?>"><button class="enigma_send_button" type="submit"><?php _e('Přejít na hlavní stránku','weblizar'); ?></button></a></p>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
