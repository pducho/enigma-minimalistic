<div class="col-md-8">
	<div class="error_404">
		<h2><?php _e('Nenalezeno','weblizar'); ?></h2>
		<h4><?php _e('Omlouváme se, ovšem vámi hledaný obsah neexistuje.','weblizar'); ?></h4>
		<p><?php _e('Omlouváme se, ale vámi hledaná stránka neexistuje.','weblizar'); ?></p>
		<p><a href="<?php echo home_url( '/' ); ?>"><button class="enigma_send_button" type="submit"><?php _e('Přejít na hlavní stránku','weblizar'); ?></button></a></p>
	</div>
</div>
